# requirez(table.awk)
BEGIN {
table(Wbs,
	"wbs,                                          factor,            CO11,CO12,CO13,CO14,CO15,               EP11,EP12, EP13,  EP14,  EP15",
	" 1.1,                 Management and Planning of IV&V,              X,  X,  X,    X,    X,                  X,    X,    X,    X,     X"\
	",1.2,                         Issue and Risk Tracking,               ,  X,  X,    X,    X,                   ,    X,    X,    X,     X"\
	",1.3,                         Final Report Generation,               ,  X,  X,    X,    X,                   ,    X,    X,    X,     X"\
	",1.4,                               IV&V Tool Support,               ,  X,  X,    X,    X,                   ,    X,    X,    X,     X"\
	",1.5,         Management and Technical Review Support,              X,  X,  X,    X,    X,                  X,    X,    X,    X,     X"\
	",1.6,                            Criticality Analysis,              X,  X,  X,    X,    X,                  X,    X,    X,    X,     X"\
	",2.1,                                 Reuse Analysis*,               ,   ,  X,    X,    X,                   ,     ,     ,     ,      "\
	",2.2,                Software Architecture Assessment,               ,   ,  X,    X,    X,                   ,     ,     ,     ,      "\
	",2.3,                      System Requirements Review,               ,   ,  X,    X,    X,                   ,     ,     ,    X,     X"\
	",2.4,                     Concept Document Evaluation,               ,   ,   ,   X^,   X^,                   ,     ,     ,     ,    X^"\
	",2.5,  Software/User Requirements Allocation Analysis,               ,   ,   ,   X^,   X^,                   ,     ,     ,     ,    X^"\
	",2.6,                           Traceability Analysis,               ,   ,   ,   X^,   X^,                   ,     ,     ,     ,    X^"\
	",3.1,            Traceability Analysis -  Requirements,               ,  X,  X,    X,    X,                   ,     ,     ,    X,    X"\
	",3.2,                Software Requirements Evaluation,               ,   ,  X,    X,    X,                   ,     ,     ,    X,     X"\
	",3.3,               Interface Analysis - Requirements,               ,   ,   ,    X,    X,                   ,     ,    X,    X,     X"\
	",3.4,                       System Test Plan Analysis,               ,   ,  X,    X,    X,                   ,     ,     ,     ,      "\
	",3.5,                   Acceptance Test Plan Analysis,               ,   ,   ,     ,    X,                   ,     ,     ,     ,      "\
	",3.6,                      Timing and Sizing Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,   X^,    X^"\
	",4.1,                  Traceability Analysis - Design,               ,   ,   ,     ,     ,                   ,    X,    X,    X,     X"\
	",4.2,                      Software Design Evaluation,               ,   ,   ,    X,    X,                   ,     ,     ,    X,     X"\
	",4.3,                     Interface Analysis - Design,               ,   ,   ,     ,    X,                   ,     ,     ,    X,     X"\
	",4.4,                      Software FQT Plan Analysis,               ,  X,  X,    X,    X,                   ,     ,     ,     ,      "\
	",4.5,         Software Integration Test Plan Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,    X,     X"\
	",4.6,                               Database Analysis,               ,   ,   ,     ,     ,                   ,     ,    X,    X,     X"\
	",4.7,                    Component Test Plan Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,     ,     X"\
	",5.1,                    Traceability Analysis - Code,               ,   ,   ,     ,    X,                   ,    X,    X,    X,     X"\
	",5.2,        Source Code and Documentation Evaluation,               ,   ,   ,    X,    X,                   ,     ,    X,    X,     X"\
	",5.3,                       Interface Analysis - Code,               ,   ,   ,    X,    X,                   ,     ,    X,    X,     X"\
	",5.4,                       System Test Case Analysis,               ,   ,   ,    X,    X,                   ,     ,     ,     ,      "\
	",5.5,                      Software FQT Case Analysis,               ,   ,   ,    X,    X,                   ,     ,     ,     ,      "\
	",5.6,         Software Integration Test Case Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,     ,     X"\
	",5.7,                   Acceptance Test Case Analysis,               ,   ,   ,     ,    X,                   ,     ,     ,     ,      "\
	",5.8,    Software Integration Test Procedure Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,     ,     X"\
	",5.9,      Software Integration Test Results Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,    X,     X"\
	",5.10,                    Component Test Case Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,     ,     X"\
	",5.11,                  System Test Procedure Analysis,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,     "\
	",5.12,                 Software FQT Procedure Analysis,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,     "\
	",6.1,                    Traceability Analysis - Test,               ,  X,  X,    X,    X,                   ,     ,     ,     ,     X"\
	",6.2,                        Regression Test Analysis,               ,   ,   ,     ,     ,                   ,     ,     ,   X^,    X^"\
	",6.3,                             Simulation Analysis,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,      "\
	",6.4,                    System Test Results Analysis,               ,   ,   ,    X,    X,                   ,     ,     ,     ,      "\
	",6.5,                   Software FQT Results Analysis,               ,   ,   ,    X,    X,                   ,     ,     ,     ,      "\
	",7.1,                  Operating Procedure Evaluation,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,      "\
	",7.2,                              Anomaly Evaluation,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,      "\
	",7.3,                            Migration Assessment,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,      "\
	",7.4,                           Retirement Assessment,               ,   ,   ,     ,   X^,                   ,     ,     ,     ,      ",
	WbsNames,WbsRows);
}
